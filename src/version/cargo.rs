use anyhow::Result;
use cargo_toml::Manifest;
use std::fs::{self, File};
use std::io::Read;

fn get_file_as_byte_vec(filename: &str) -> Option<Vec<u8>> {
    let f = File::open(&filename);
    if f.is_err() {
        log::debug!("can't read from Cargo.toml: {:?}", f.unwrap_err());
        return None;
    }
    let mut f = f.unwrap();
    let metadata = fs::metadata(&filename).expect("unable to read metadata");
    let mut buffer = vec![0; metadata.len() as usize];
    f.read(&mut buffer).expect("buffer overflow");

    Some(buffer)
}

pub fn read() -> Result<Option<String>> {
    log::debug!("reading version from Cargo.toml");
    let bytes = get_file_as_byte_vec("Cargo.toml");
    log::debug!("{:?}", bytes);

    match bytes {
        Some(bytes) => {
            log::trace!("reading toml");
            let pkg : Result<Manifest, _> = toml::from_slice(&bytes);
            match pkg {
                Err(why) => {
                    log::error!("error parsing Cargo.toml: {:?}", why);
                    Err(why.into())
                }
                Ok(pkg) => {
                    let version = pkg.package.unwrap().version;
                    log::trace!("got version {}", version);
                    Ok(Some(version))
                }
            }
        }
        None => Ok(None)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn read() {
        use super::read;
        let _ = pretty_env_logger::try_init();
        read().unwrap().unwrap();
    }
}
