let
  sources = import ./nix/sources.nix;
  pkgs = import <nixpkgs> { overlays = [ (import sources.nixpkgs-mozilla) ]; };
in pkgs.mkShell {
  buildInputs = with pkgs; [
    latest.rustChannels.stable.rust
    cargo-watch
    openssl
    pkg-config
    libgit2
  ];
}
