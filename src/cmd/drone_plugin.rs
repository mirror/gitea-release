use crate::cmd::*;
use anyhow::Result;
use git2::Repository;
use url::Url;

pub async fn run(env: DroneEnv) -> Result<()> {
    let common: Common = env.clone().into();

    let default_branch = {
        let common = common.clone();
        match &env.default_branch {
            None => {
                let cli =
                    gitea::Client::new(common.server, common.token, crate::APP_USER_AGENT)?;
                let repo = cli.get_repo(common.owner, common.repo).await?;
                repo.default_branch
            }
            Some(branch) => branch.to_string(),
        }
    };

    if env.branch != default_branch {
        return Ok(());
    }

    let repo = Repository::open(".")?;
    let mut u = Url::parse(&env.push_url)?;
    u.set_username(&env.auth_user).unwrap();
    u.set_password(Some(&env.token)).unwrap();
    repo.remote_delete("origin")?;
    let mut origin = repo.remote("origin", u.as_str())?;
    origin.connect(git2::Direction::Fetch)?;
    origin.fetch(&["refs/tags/*:refs/tags/*"], None, None)?;

    cut::run(
        common,
        env.changelog_path,
        None,
        ReleaseMeta {
            name: None,
            draft: false,
            pre_release: false,
        },
    )
    .await
}
