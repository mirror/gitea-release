# elfs

A simple name generator based on [Pokemon Vietnamese Crystal](https://tvtropes.org/pmwiki/pmwiki.php/VideoGame/PokemonVietnameseCrystal).

## Usage

Add the following to your `Cargo.toml`:

```toml
[dependencies]
elfs = "0.1"
```

```rust
let name = elfs::next();
```
