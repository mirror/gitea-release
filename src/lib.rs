pub mod changelog;
pub mod cmd;
pub mod git;
pub mod version;

// Name your user agent after your app?
pub static APP_USER_AGENT: &str =
    concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));
