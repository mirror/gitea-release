use crate::{changelog, cmd::*, git, version};
use anyhow::Result;
use std::path::PathBuf;

pub async fn run(
    common: Common,
    fname: PathBuf,
    tag: Option<String>,
    rm: ReleaseMeta,
) -> Result<()> {
    let repo = git2::Repository::open(".")?;
    let tag = tag.unwrap_or(version::read("VERSION".into())?);
    let vtag = format!("v{}", tag);

    if git::has_tag(&repo, vtag.clone())? {
        println!("release {} already released", vtag);
        return Ok(());
    }

    let desc = changelog::read(fname, tag.clone())?;
    let cli = gitea::Client::new(common.server, common.token, crate::APP_USER_AGENT)?;
    let repo = cli
        .get_repo(common.owner.clone(), common.repo.clone())
        .await?;

    let cr = gitea::CreateRelease {
        body: desc,
        draft: rm.draft,
        name: rm.name.or(Some(format!("Version {}", tag))).unwrap(),
        prerelease: rm.pre_release,
        tag_name: vtag,
        target_commitish: repo.default_branch,
    };

    let _ = cli.create_release(common.owner, common.repo, cr).await?;

    println!("Created release {}", tag);

    Ok(())
}
