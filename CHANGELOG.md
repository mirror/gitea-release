# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 0.7.1

Fix a minor parsing bug

## 0.7.0

Support reading version data from `Cargo.toml`

## 0.6.1

Fixed a logic error in release cutting.

## 0.5.2

Detect tags better.

## 0.5.1

Initialize `pretty_env_logger` on app boot. Oops.

## 0.5.0

A lot of internals to this program were exposed to external consumers. This was
used to provide an integration test.

Support for bracketed versions was added, a-la:

```markdown
## [0.1.0]

Hi there this is a test!

### ADDED

- something
```

The gitea crate was brought up to version `0.2.0` and includes a lot more
functionality.

## 0.4.0

This is a functionality-breaking release that removes untested/extraneous parts
of this project.

### ADDED

- The gitea client embedded into this repo is now a more generic crate that can
  be reused across other projects.
- Gitea types have been simplified and redundant types have been removed.
- Simple tests for the gitea crate.
- A name generator `elfs` was created for future use in integration testing.

### CHANGED

- `release` is renamed to `cut`, but there is an alias for the old `release`
  subcommand name.

### REMOVED

- All functionality but the drone plugin and release commands.

## 0.3.2

Automagically fetch tags when running as a drone plugin.

## 0.3.1

Hotfix in json parsing

## 0.3.0

Allows for a customizable default branch name for the drone plugin using either
a hard-coded value or the Gitea api to fetch it. For compatibility's sake, the
default behavior is to fetch the default branch name from the Gitea api. If you
need to hard-code your default branch name, add the config like this:

```yaml
  - name: auto-release
    image: xena/gitea-release:0.3.1
    settings:
      auth_username: cadey
      default_branch: trunk
      gitea_server: https://tulpa.dev
      gitea_token:
        from_secret: GITEA_TOKEN
    when:
      event:
        - push
      branch:
        - trunk
```

Also fixed a suggestion to fetch tags over git before trying to run this in Drone.

## 0.2.7

### FIXED

Exit if this version already exists

## 0.2.6

A fix from @kivikakk to remove the use of RefCells in markdown parsing

## 0.2.5

CD fix

## 0.2.4

I need to make some kind of drone CI validator

## 0.2.3

Typo in the CD script :D

## 0.2.2

Update README, automatically push docker images

## 0.2.1

Hotfix for typos in the docker manifest.

## 0.2.0

### ADDED

- Added [Drone plugin](https://docs.drone.io/plugins/overview/) support

### Drone Plugin Support

To use this as a drone plugin, use the following config:

```yaml
- name: auto-release
  image: xena/gitea-release:0.2.0
  settings:
    auth_username: cadey
    gitea_server: https://tulpa.dev
    gitea_token:
      from_secret: GITEA_TOKEN
  when:
    branch:
      include:
        - master
```

## 0.1.1

### FIXED

- Fixed 500 error when creating a new release

## 0.1.0

This is the first release of `gitea-release`!

```console
$ gitea-release release --help
gitea-release-release 0.1.0
Create a new tag and release on Gitea

USAGE:
    gitea-release release [FLAGS] [OPTIONS] --auth-user <auth-user> --owner <owner> --repo <repo> --server <server> --token <token>

FLAGS:
        --draft          Draft release
    -h, --help           Prints help information
    -p, --pre-release    Pre-release (not suitable for production)
    -V, --version        Prints version information

OPTIONS:
    -a, --auth-user <auth-user>    The gitea user to authenticate as [env: GITEA_AUTH_USER=cadey]
    -c, --changelog <changelog>    Changelog file to read from to create the release description [default:
                                   ./CHANGELOG.md]
    -n, --name <name>              Release name
    -o, --owner <owner>            The owner of the gitea repo [env: GITEA_OWNER=]
    -r, --repo <repo>              The gitea repo to operate on [env: GITEA_REPO=]
    -s, --server <server>          The gitea server to connect to [env: GITEA_SERVER=https://tulpa.dev]
        --token <token>            The gitea token to authenticate with [env: GITEA_TOKEN=]
```

### ADDED

- Basic functionality

