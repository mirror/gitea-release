{ system ? builtins.currentSystem }:

let
  pkgs = import <nixpkgs> { };
  callPackage = pkgs.lib.callPackageWith pkgs;
  gitea-release = callPackage ./default.nix { };

  dockerImage = pkg:
    pkgs.dockerTools.buildLayeredImage {
      name = "xena/gitea-release";
      tag = "latest";

      contents = [ pkgs.cacert pkg ];

      config = {
        Cmd = [ "/bin/gitea-release" "drone-plugin" ];
        WorkingDir = "/";
      };
    };

in dockerImage gitea-release
