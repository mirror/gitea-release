use anyhow::Result;
use std::{fs, path::PathBuf};

mod cargo;

pub(crate) fn read(fname: PathBuf) -> Result<String> {
    let version = match read_fs(fname.clone()) {
        Ok(version) => version,
        Err(why) => {
            log::debug!("can't read {:?}: {:?}", fname, why);
            cargo::read().unwrap().unwrap()
        }
    };
    Ok(version)
}

fn read_fs(fname: PathBuf) -> Result<String> {
    log::debug!("reading version data from {:?}", fname);
    Ok(fs::read_to_string(fname)?.trim().into())
}

#[cfg(test)]
mod tests {
    #[test]
    fn read_version() {
        let _ = pretty_env_logger::try_init();
        let version = super::read_fs("./testdata/VERSION".into()).unwrap();
        assert_eq!(version, "0.1.0");
    }
}
