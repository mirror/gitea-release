use anyhow::Result;
use structopt::StructOpt;

use ::gitea_release::{cmd::{self, Cmd}};

#[tokio::main]
async fn main() -> Result<()> {
    let _ = kankyo::init();
    pretty_env_logger::init();
    let cmd = Cmd::from_args();

    match cmd {
        Cmd::Cut {
            common,
            changelog,
            tag,
            release_meta,
        } => cmd::cut::run(common, changelog, tag, release_meta).await,
        Cmd::DronePlugin { env } => cmd::drone_plugin::run(env).await,
    }
}
