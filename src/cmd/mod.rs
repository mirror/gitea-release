use std::path::PathBuf;
use structopt::StructOpt;

pub mod cut;
pub mod drone_plugin;

#[derive(StructOpt, Debug, Clone)]
pub struct Common {
    /// The gitea server to connect to
    #[structopt(short, long, env = "GITEA_SERVER")]
    pub server: String,

    /// The gitea token to authenticate with
    #[structopt(long, env = "GITEA_TOKEN")]
    pub token: String,

    /// The gitea user to authenticate as
    #[structopt(short, long, env = "GITEA_AUTH_USER")]
    pub auth_user: String,

    /// The owner of the gitea repo
    #[structopt(short, long, env = "GITEA_OWNER")]
    pub owner: String,

    /// The gitea repo to operate on
    #[structopt(short, long, env = "GITEA_REPO")]
    pub repo: String,

    /// Git signature email
    #[structopt(long, short = "E", env = "SIGNATURE_EMAIL", default_value = "domo@tulpa.dev")]
    pub email: String,

    /// Git signature username
    #[structopt(long, short = "U", env = "SIGNATURE_NAME", default_value = "Domo Arigato")]
    pub username: String,
}

#[derive(StructOpt, Debug, Clone)]
pub struct DroneEnv {
    // Given by drone
    /// push URL
    #[structopt(long, env = "DRONE_GIT_HTTP_URL")]
    pub push_url: String,

    /// repo owner
    #[structopt(long, env = "DRONE_REPO_OWNER")]
    pub owner: String,

    /// repo name
    #[structopt(long, env = "DRONE_REPO_NAME")]
    pub repo: String,

    /// branch
    #[structopt(long, env = "DRONE_REPO_BRANCH")]
    pub branch: String,

    // Given by the user
    /// auth username
    #[structopt(long, env = "PLUGIN_AUTH_USERNAME")]
    pub auth_user: String,

    /// Gitea server
    #[structopt(long, env = "PLUGIN_GITEA_SERVER")]
    pub server: String,

    /// Gitea token
    #[structopt(long, env = "PLUGIN_GITEA_TOKEN")]
    pub token: String,

    /// CHANGELOG path
    #[structopt(long, env = "PLUGIN_CHANGELOG_PATH", default_value = "./CHANGELOG.md")]
    pub changelog_path: PathBuf,

    /// Default branch name
    #[structopt(long, env = "PLUGIN_DEFAULT_BRANCH")]
    pub default_branch: Option<String>,

    /// Git signature email
    #[structopt(long, short = "E", env = "PLUGIN_SIGNATURE_EMAIL", default_value = "domo@tulpa.dev")]
    pub email: String,

    /// Git signature username
    #[structopt(long, short = "U", env = "PLUGIN_SIGNATURE_NAME", default_value = "Domo Arigato")]
    pub username: String,
}

impl Into<Common> for DroneEnv {
    fn into(self) -> Common {
        Common {
            server: self.server,
            token: self.token,
            auth_user: self.auth_user,
            owner: self.owner,
            repo: self.repo,
            email: self.email,
            username: self.username,
        }
    }
}

#[derive(StructOpt, Debug)]
pub struct ReleaseMeta {
    /// Release name
    #[structopt(short, long)]
    pub name: Option<String>,
    /// Draft release
    #[structopt(long)]
    pub draft: bool,
    /// Pre-release (not suitable for production)
    #[structopt(short, long)]
    pub pre_release: bool,
}

#[derive(StructOpt, Debug)]
#[structopt(about = "Gitea release assistant")]
pub enum Cmd {
    /// Create a new tag and release on Gitea
    #[structopt(alias = "release")]
    Cut {
        #[structopt(flatten)]
        common: Common,
        /// Changelog file to read from to create the release description
        #[structopt(short, long, default_value = "./CHANGELOG.md")]
        changelog: PathBuf,
        /// The version tag to operate on
        tag: Option<String>,
        #[structopt(flatten)]
        release_meta: ReleaseMeta,
    },

    /// Runs the release process as a drone plugin
    DronePlugin {
        #[structopt(flatten)]
        env: DroneEnv,
    },
}
