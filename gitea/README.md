# gitea

A simple Gitea client for Rust programs. You will need an API token as described
[here](https://docs.gitea.io/en-us/api-usage/).

```toml
gitea = "0.1.0"
```
