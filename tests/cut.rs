use ::gitea_release::{cmd, git, APP_USER_AGENT};
use anyhow::Result;
use git2::{Repository, Signature};
use log::debug;
use std::{fs::File, io::Write};

const TAG: &'static str = "0.1.0";

#[tokio::test]
async fn cut() -> Result<()> {
    let _ = pretty_env_logger::try_init();
    let name = elfs::next();
    let token = std::env::var("DOMO_GITEA_TOKEN").expect("wanted envvar DOMO_GITEA_TOKEN");

    let cli = gitea::Client::new("https://tulpa.dev".into(), token.clone(), APP_USER_AGENT)?;
    debug!("created gitea client");

    let gitea_repo = cli
        .create_user_repo(gitea::CreateRepo {
            auto_init: false,
            description: format!("https://tulpa.dev/cadey/gitea-release test repo"),
            gitignores: "".into(),
            issue_labels: "".into(),
            license: "".into(),
            name: name.clone(),
            private: true,
            readme: "".into(),
        })
        .await?;
    debug!("created repo domo/{}", name);

    let dir = tempfile::tempdir()?;
    let repo = Repository::init(&dir)?;
    let sig = &Signature::now("Domo Arigato", "domo@tulpa.dev")?;
    debug!("initialized repo in {:?}", dir.path());

    let mut fout = File::create(&dir.path().join("VERSION"))?;
    write!(fout, "{}", TAG)?;
    drop(fout);

    let mut fout = File::create(&dir.path().join("CHANGELOG.md"))?;
    fout.write_all(include_bytes!("../testdata/basic.md"))?;
    drop(fout);

    git::commit(&repo, &sig, TAG.into(), &["VERSION", "CHANGELOG.md"])?;
    debug!("committed files");

    repo.remote("origin", &gitea_repo.clone_url)?;
    debug!("set up origin remote to {}", gitea_repo.clone_url);

    git::push(
        &repo,
        token.clone(),
        &["refs/heads/master:refs/heads/master"],
    )?;
    debug!("pushed to {} with token {}", gitea_repo.clone_url, token);

    std::env::set_current_dir(dir.path())?;
    cmd::cut::run(
        cmd::Common {
            server: "https://tulpa.dev".into(),
            token: token,
            auth_user: gitea_repo.owner.login.clone(),
            owner: gitea_repo.owner.login.clone(),
            repo: gitea_repo.name,
            email: "domo@tulpa.dev".into(),
            username: "Domo Arigato".into(),
        },
        "CHANGELOG.md".into(),
        None,
        cmd::ReleaseMeta {
            name: None,
            draft: false,
            pre_release: false,
        },
    )
    .await?;

    let rls = cli
        .get_release_by_tag(
            gitea_repo.owner.login.clone(),
            name.clone(),
            format!("v{}", TAG),
        )
        .await?;

    assert_eq!(
        rls.body,
        "Hi there this is a test\\!\n### ADDED\n  - something\n"
    );

    cli.delete_repo(gitea_repo.owner.login, name).await?;
    debug!("deleted repo {}", gitea_repo.clone_url);

    Ok(())
}
