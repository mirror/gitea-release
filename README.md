# gitea-release

[![built with nix](https://builtwithnix.org/badge.svg)](https://builtwithnix.org)
[![Build Status](https://drone.tulpa.dev/api/badges/cadey/gitea-release/status.svg)](https://drone.tulpa.dev/cadey/gitea-release)

A small command line tool to automate releases for [Gitea](https://gitea.io)
repositories that reads from CHANGELOG and VERSION files. This is a clone of
[github-release](https://github.com/github-release/github-release), but more
suited for my individual needs. This may also turn into a generic webhook
handler, but one thing at a time. :)

## Installation

### With Nix

```console
$ nix-env -if https://tulpa.dev/cadey/gitea-release/archive/master.tar.gz
```

### With cargo

```console
$ cargo install --git https://tulpa.dev/cadey/gitea-release.git
```

## Drone Plugin

To use this as a drone plugin, add the following to your `.drone.yml` under the
`steps` key:

```yaml
- name: auto-release
  image: xena/gitea-release:latest
  pull: always
  settings:
    auth_username: cadey
    changelog_path: ./CHANGELOG.md
    gitea_server: https://tulpa.dev
    gitea_token:
      from_secret: GITEA_TOKEN
  when:
    event:
      - push
    branch:
      - master
```

Replace the values of the settings as makes sense for your gitea server. The
`changelog_path` attribute is optional, and will be `./CHANGELOG.md` by default.

The default branch will automatically be derived from the Gitea API. If you need
to hard-code your default branch name for some reason, add the `default_branch`
setting like this:

```yaml
  - name: auto-release
    image: xena/gitea-release:latest
    pull: always
    settings:
      auth_username: cadey
      default_branch: trunk
      gitea_server: https://tulpa.dev
      gitea_token:
        from_secret: GITEA_TOKEN
    when:
      event:
        - push
      branch:
        - trunk
```

## CHANGELOG.md and VERSION files

The `CHANGELOG.md` file is based on the [Keep a Changelog][kacl] format, but
modified slightly to make it easier for this tool. Here is an example changelog
that this tool accepts:

[kacl]: https://keepachangelog.com/en/1.0.0/

```markdown
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.1.0

Hi there this is a test!

### ADDED

- something
```

The `VERSION` file plays into this as well. The `VERSION` file MUST be a single
line containing a [semantic version][semver] string. When this tool is run with
the `release` subcommand, the following actions take place:

[semver]: https://semver.org/spec/v2.0.0.html

- The `VERSION` file is read and loaded as the desired tag for the repo
- The `CHANGELOG.md` file is read and the changes for the `VERSION` are
  cherry-picked out of the file
- The git repo is checked to see if that tag already exists
  - If the tag exists, the tool exits and does nothing
- If the tag does not exist, it is created (with the changelog fragment as the
  body of the tag) and pushed to the gitea server
- A gitea release is created using the changelog fragment and the release name
  is generated from the `VERSION` string
